#include "DuckScene.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Scripts/DuckController.h"

namespace DucklingsEngine
{
	void DuckScene::Start()
	{
		// This will be execute when loading the scene.
		GameObject *duck = new GameObject();
		duck->AddComponent<SpriteRenderer>("penguin");

		duck->AddComponent<DuckController>();

	}
}