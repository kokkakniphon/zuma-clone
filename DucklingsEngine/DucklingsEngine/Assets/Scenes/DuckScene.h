#pragma once

#include "Scene.h"
#include "Engine.h"

namespace DucklingsEngine
{
	class DuckScene : public Scene
	{
	public:
		DuckScene() { name = "DuckScene"; }
		~DuckScene() = default;

		void Start();

		inline void LoadScene()
		{
			Scene::LoadScene();
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			Engine::GetInstance().manager->Start();
		}

		inline bool UnloadScene()
		{
			std::cout << "Unload Scene: " << name << std::endl;
			for (auto object : *GetRootsGameObject())
			{
				std::cout << "Unload Object: " << object->name << std::endl;
				Engine::GetInstance().manager->EraseEntity(object->GetUniqueID());
			}

			GetRootsGameObject()->clear();

			return Scene::UnloadScene();
		}
	};
}

