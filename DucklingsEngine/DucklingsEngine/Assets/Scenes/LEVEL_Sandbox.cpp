#include "LEVEL_Sandbox.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Rigidbody.h"
#include "Scripts/PenguinController.h"

#include "Transform.h"

namespace DucklingsEngine
{

	void LEVEL_Sandbox::Start()
	{
		AssetManager::GetInstance().LoadTexture("penguin", "../Resource/Texture/penguin.png");
		AssetManager::GetInstance().LoadTexture("bg", "../Resource/Texture/background.png");

		bool addedForce = false;

		GameObject* background = new GameObject();
		background->name = "Background";
		background->transform->SetPosition(Vector2(960, 540));
		background->AddComponent<SpriteRenderer>("bg");

		GameObject* penguin;
		penguin = new GameObject("BIG Penguin");
		penguin->transform->SetPosition(Vector2(960, 540));

		Sprite* penguin_sprite = new Sprite("penguin");
		penguin->AddComponent<SpriteRenderer>(penguin_sprite);

		penguin->AddComponent<Rigidbody>().AddForce(Vector2(0, -50));

		penguin->AddComponent<PenguinController>();

	}

}
