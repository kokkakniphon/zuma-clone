#include "PhysicTestScene.h"

#include "GameObject.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "CircleCollider.h"
#include "EdgeCollider.h"
#include "BoxCollider.h"
#include "Scripts/EdgeController.h"
#include "AnimationController/ZumaController.h"
#include "Animator.h"

namespace DucklingsEngine
{
	void PhysicTestScene::Start()
	{
		// This will be execute when loading the scene.

		ZumaController *animation = new ZumaController();

		GameObject* ball1 = new GameObject(" Ball One ");
		//GameObject* ball2 = new GameObject(" Ball Two ");
		GameObject* ball3 = new GameObject(" Ball Three ");
		GameObject* line1 = new GameObject(" Line One ");
		GameObject* line2 = new GameObject(" Line Two ");
		GameObject* box1 = new GameObject(" Box One ");

		ball1->transform->SetPosition(Vector2(760, 440));
		ball1->transform->localScale = Vector2(5.0f, 5.0f);
		//ball2->transform->SetPosition(Vector2(1060, 540));
		//ball2->transform->localScale = Vector2(0.5f, 0.5f);
		ball3->transform->SetPosition(Vector2(1300, 340));
		ball3->transform->localScale = Vector2(0.5f, 0.5f);

		line1->transform->SetPosition(Vector2(760, 740));
		line2->transform->SetPosition(Vector2(1920 - 760, 740));
		box1->transform->SetPosition(Vector2(1920 / 2, 0));

		ball1->AddComponent<Rigidbody>().gravityScale;
		ball3->AddComponent<Rigidbody>().gravityScale;
		box1->AddComponent<Rigidbody>().gravityScale;

		ball1->GetComponent<Rigidbody>().mass = 1.0f;
		ball3->GetComponent<Rigidbody>().mass = 1.0f;
		//box1->GetComponent<Rigidbody>().mass = 1.0f;

		ball1->AddComponent<SpriteRenderer>();
		ball1->AddComponent<CircleCollider>().radius = 150.0f;
		//ball2->AddComponent<SpriteRenderer>("ball");
		//ball2->AddComponent<CircleCollider>().radius = 100.0f;
		ball3->AddComponent<SpriteRenderer>("ball");
		ball3->AddComponent<CircleCollider>().radius = 100.0f;

		box1->transform->localScale = Vector2(5.0f, 5.0f);

		line1->AddComponent<BoxCollider>().transform->localScale = Vector2(500, 10);
		line1->transform->localEulerAngles = 30;

		line2->AddComponent<BoxCollider>().transform->localScale = Vector2(500, 10);
		line2->transform->localEulerAngles = -30;

		box1->AddComponent<BoxCollider>().size = Vector2(20, 20);

		//line1->GetComponent<BoxCollider>().points.push_back(Vector2(0,0));
		//line1->GetComponent<BoxCollider>().points.push_back(Vector2(100, 0));
		//line1->GetComponent<BoxCollider>().points.push_back(Vector2(500, 0));

		//line1->GetComponent<EdgeCollider>().points.push_back(Vector2(0,0));
		//line1->GetComponent<EdgeCollider>().points.push_back(Vector2(100, 0));
		//line1->GetComponent<EdgeCollider>().points.push_back(Vector2(500, 0));
	}
}