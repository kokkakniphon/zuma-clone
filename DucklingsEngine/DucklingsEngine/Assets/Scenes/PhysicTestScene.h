#pragma once

#include "Scene.h"
#include "Engine.h"

namespace DucklingsEngine
{
	class PhysicTestScene : public Scene
	{
	public:
		PhysicTestScene() { name = "PhysicTestScene"; }
		~PhysicTestScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			Engine::GetInstance().manager->Start();
		}

		inline bool UnloadScene()
		{
			std::cout << "Unload Scene: " << name << std::endl;
			for (auto object : *GetRootsGameObject())
			{
				Engine::GetInstance().manager->EraseEntity(object->GetUniqueID());
			}

			GetRootsGameObject()->clear();

			return Scene::UnloadScene();
		}
	};
}

