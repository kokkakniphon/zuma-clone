#include "Zuma.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Animator.h"

#include "BoxCollider.h"

#include "AnimationController/BallAnim.h"
#include "Scripts/BallController.h"
#include "Scripts/BallPathController.h"
#include "Scripts/FrogController.h"
#include "Scripts/PathCreator.h"

namespace DucklingsEngine
{
	void Zuma::Start()
	{
		// This will be execute when loading the scene.

		// ---------- Level Setup ----------
		GameObject* level_bg = new GameObject();
		level_bg->transform->SetPosition(Vector2((float)SCREEN_WIDTH / 2, (float)SCREEN_HEIGHT / 2));
		level_bg->AddComponent<SpriteRenderer>("levels/Spiral");

		GameObject* level_hole = new GameObject("Hole");
		level_hole->transform->SetPosition(Vector2((float)SCREEN_WIDTH / 2 - 105, (float)SCREEN_HEIGHT / 2 - 25));
		level_hole->AddComponent<SpriteRenderer>("Hole");

		// ---------------------------------

		GameObject* pathCreator = new GameObject();
		pathCreator->transform->SetPosition(Vector2((float)SCREEN_WIDTH / 2, (float)SCREEN_HEIGHT / 2));
		pathCreator->AddComponent<PathCreator>().currentPathName = "Spiral";

		GameObject* ballPathController = new GameObject();
		ballPathController->AddComponent<BallPathController>().pathCreator = &pathCreator->GetComponent<PathCreator>();




		// ------ Frog Setup -----------
		GameObject* frog = new GameObject();
		frog->transform->SetPosition(Vector2((float)SCREEN_WIDTH / 2, (float)SCREEN_HEIGHT / 2));
		frog->AddComponent<FrogController>().path = &ballPathController->GetComponent<BallPathController>();
		// -----------------------------

		const int numberOfBalls = 150;
		for (int i = 0; i < numberOfBalls; i++)
		{
			GameObject* ball = new GameObject("baBall_" + std::to_string(i));
			ball->tag = "baBall";
			ball->transform->SetPosition(Vector2(20.0f, 20.0f));
			ball->AddComponent<SpriteRenderer>("baBallRed");
			ball->AddComponent<Animator>();
			ball->AddComponent<CircleCollider>().radius = 15;
			ball->GetComponent<CircleCollider>().isTrigger = true;
			ball->AddComponent<BallController>().ballColor = static_cast<BallColor>(std::rand() % 4);

			ballPathController->GetComponent<BallPathController>().AddBall(&ball->GetComponent<BallController>());
		}
	}
}