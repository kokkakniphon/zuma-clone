#pragma once
#include "MonoBehaviour.h"

#include "Animator.h"

#include "AnimationController/BlueBallAnim.h"
#include "AnimationController/GreenBallAnim.h"
#include "AnimationController/RedBallAnim.h"
#include "AnimationController/YellowBallAnim.h"

#include "CircleCollider.h"

namespace DucklingsEngine
{
	enum BallColor
	{
		Blue = 0,
		Green = 1,
		Red = 2,
		Yellow = 3
	};

	class BallController : public MonoBehaviour
	{
	private:

		float startMoveTime, currentMoveTime;
		Vector2 startPos, destination;
	public:
		BallController() = default;
		~BallController() = default;

		void Start() override;
		void Update() override;

		BallColor ballColor;

		bool isInPath = false;
		bool checkMatch = false;
		bool isChecked = false;
		float progression;

		bool connectToFront = false;

		void SetBallColor(BallColor color);
		void MoveToDestination(Vector2 destination, float seconds);
		bool CheckEndOfAnimation();

		Animator* anim;
		
	};

	void BallController::Start()
	{
		// This will execute once at the begginning of the scene.
		anim = &this->gameObject->GetComponent<Animator>();
		SetBallColor(ballColor);
	}

	void BallController::Update()
	{
		if (isInPath)
		{
			if (currentMoveTime > 0)
			{
				currentMoveTime -= Time::GetDeltaTime();
				Vector2 thisPos = this->transform->GetPosition();
				this->transform->SetPosition(Vector2::Lerp(thisPos, destination, (startMoveTime - currentMoveTime) / startMoveTime));
			}
			else
			{
				this->transform->SetPosition(destination);
			}
		}
		
	}



	void BallController::SetBallColor(BallColor color)
	{
		ballColor = color;

		switch (color)
		{
		case DucklingsEngine::Blue:
			anim->SetAnimationController(std::make_shared<BlueBallAnim>());
			break;
		case DucklingsEngine::Green:
			anim->SetAnimationController(std::make_shared<GreenBallAnim>());
			break;
		case DucklingsEngine::Red:
			anim->SetAnimationController(std::make_shared<RedBallAnim>());
			break;
		case DucklingsEngine::Yellow:
			anim->SetAnimationController(std::make_shared<YellowBallAnim>());
			break;
		default:
			break;
		}
	}

	void BallController::MoveToDestination(Vector2 destination, float seconds = -1)
	{
		if (seconds != -1)
		{
			startMoveTime = seconds;
			currentMoveTime = seconds;
		}
		this->destination = destination;

	}

	bool BallController::CheckEndOfAnimation()
	{
		return (currentMoveTime <= 0);
	}
}

