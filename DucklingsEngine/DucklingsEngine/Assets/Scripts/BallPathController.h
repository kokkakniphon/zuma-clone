#pragma once
#include "MonoBehaviour.h"

#include "BallController.h"
#include "PathCreator.h"

#include "Input.h"

namespace DucklingsEngine
{
	class BallPathController : public MonoBehaviour
	{
	private:
		float ballRadius;
		std::vector<BallController*> balls;
		std::vector<BallController*> matchBalls;

		int currentHeadIndex = 0;

		float currentSpeed = 2.0f;
		float normalSpeed = 2.0f;
		float speedUpDuration = 1.0f;
		float speedUp = 15.0f;

	public:
		BallPathController() = default;
		~BallPathController() = default;

		void Start() override;
		void Update() override;

		PathCreator* pathCreator;
		bool started = true;

		void AddBall(BallController* ball);

		void SetAllBall();
		void CheckNeighbor(int checkIndex, BallColor checkColor);

		int RemoveBall(int uniqueID);
	};

	void BallPathController::Start()
	{
		// This will execute once at the begginning of the scene.
		if(balls.size() > 0)
			ballRadius = 15 * 2;

		std::vector<Vector2> points = pathCreator->GetCalculatedPath();
		float segmentLength = Vector2::Distance(points[0], points[1]);
		float ballLengthInSegment = ballRadius / segmentLength;

		for (size_t i = 0; i < balls.size(); i++)
		{
			balls[i]->progression = -ballLengthInSegment * i;
			balls[i]->connectToFront = true;
		}

		balls[0]->connectToFront = false;
	}

	void BallPathController::Update()
	{
		// This will execute every frame in update.
		if (Input::GetKeyDown(SDL_SCANCODE_G))
		{
			started = !started;
		}

		if (started)
		{
			if (speedUpDuration > 0)
			{
				speedUpDuration -= Time::GetDeltaTime();
				currentSpeed = speedUp - (1.0f - speedUpDuration) * (speedUp - normalSpeed);
			}
			else
			{
				currentSpeed = normalSpeed;
			}
		}

		SetAllBall();

	}

	void BallPathController::AddBall(BallController * ball)
	{
		ball->isInPath = true;
		balls.push_back(ball);
	}

	void BallPathController::SetAllBall()
	{
		std::vector<Vector2> points = pathCreator->GetCalculatedPath();
		float segmentLength = Vector2::Distance(points[0], points[1]);
		float ballLengthInSegment = ballRadius / segmentLength;

		for (unsigned int i = 0; i < balls.size(); i++)
		{
			balls[i]->gameObject->enabled = false;
			balls[i]->anim->SetPlay(false);
		}

		// Check for reconnection
		if (currentHeadIndex > 0)
		{
			if (balls[currentHeadIndex - 1]->progression - balls[currentHeadIndex]->progression < ballLengthInSegment)
			{
				balls[currentHeadIndex]->connectToFront = true;
				balls[currentHeadIndex]->progression = balls[currentHeadIndex - 1]->progression - ballLengthInSegment;

				for (int k = currentHeadIndex; k >= 0; k--)
				{
					balls[k]->progression = balls[k + 1]->progression + ballLengthInSegment;

					// Check for reconnection
					if (k > 0)
					{
						if (balls[k - 1]->progression - balls[k]->progression < ballLengthInSegment)
						{
							balls[k]->progression = balls[k - 1]->progression - ballLengthInSegment;
							balls[k]->connectToFront = true;
						}
					}

					if (balls[k]->connectToFront == false)
						break;
				}
				currentHeadIndex--;
			}
		}

		for (unsigned int i = 0; i < balls.size(); i++)
		{
			if (started && i >= currentHeadIndex)
			{
				balls[i]->progression += Time::GetDeltaTime() * currentSpeed;
				balls[i]->anim->SetPlay(true);
			}

			bool endOfPath = false;
			if (balls[i]->progression >= points.size() - 1)
			{
				balls[i]->progression = points.size() - 1;
				endOfPath = true;
				currentSpeed = 100;
			}

			if (balls[i]->progression <= 0)
				continue;

			balls[i]->gameObject->enabled = true;

			if (endOfPath == false)
			{
				balls[i]->isChecked = false;

				float currentProgressionInSegment = balls[i]->progression - std::floor(balls[i]->progression);
				Vector2 newPosition = Vector2::Lerp(points[std::floor(balls[i]->progression)], points[std::ceil(balls[i]->progression)], currentProgressionInSegment);
				balls[i]->MoveToDestination(newPosition);

				Vector2 segmentDir = points[std::ceil(balls[i]->progression)] - points[std::floor(balls[i]->progression)];
				balls[i]->transform->localEulerAngles = (atan2(segmentDir.y, segmentDir.x) * 180 / M_PI) - 90;
			
				std::vector<Collider*> collide = balls[i]->gameObject->GetComponent<CircleCollider>().collidedTriggers;

				if (collide.size() > 0 && balls[i]->gameObject->tag == "baBall")
				{
					for (size_t j = 0; j < collide.size(); j++)
					{
						// If collide with shoot ball
						if (collide[j]->gameObject->tag == "ShootBall")
						{
							collide[j]->gameObject->tag = "baBall";
							collide[j]->gameObject->GetComponent<Rigidbody>().enabled = false;
							collide[j]->gameObject->GetComponent<BallController>().isInPath = true;
							collide[j]->gameObject->GetComponent<BallController>().isChecked = false;
							collide[j]->gameObject->GetComponent<BallController>().checkMatch = true;
							collide[j]->gameObject->GetComponent<CircleCollider>().isTrigger = true;
							collide[j]->gameObject->GetComponent<BallController>().MoveToDestination(newPosition, 0.5f);

							Vector2 newBallToPathBall = collide[j]->transform->GetPosition() - balls[i]->transform->GetPosition();
							newBallToPathBall = newBallToPathBall + (collide[j]->gameObject->GetComponent<Rigidbody>().velocity / collide[j]->gameObject->GetComponent<Rigidbody>().velocity.magnitude()) * collide[j]->gameObject->GetComponent<CircleCollider>().radius;

							float dotOfBall = Vector2::Dot(segmentDir, newBallToPathBall);
							int insertIndex = (dotOfBall > 0) ? i : i + 1;
							insertIndex = insertIndex % (balls.size() - 1);

							if (balls[insertIndex]->connectToFront == false)
							{
								if (insertIndex != i)
								{
									collide[j]->gameObject->GetComponent<BallController>().connectToFront = true;
									collide[j]->gameObject->GetComponent<BallController>().progression = balls[insertIndex-1]->progression - ballLengthInSegment;
								}
								else
								{
									balls[insertIndex]->connectToFront = true;
									collide[j]->gameObject->GetComponent<BallController>().connectToFront = false;
									collide[j]->gameObject->GetComponent<BallController>().progression = balls[insertIndex]->progression;

								}
							}
							else
							{
								collide[j]->gameObject->GetComponent<BallController>().connectToFront = true;
								collide[j]->gameObject->GetComponent<BallController>().progression = balls[insertIndex]->progression;
							}


							balls.insert(balls.begin() + insertIndex, &collide[j]->gameObject->GetComponent<BallController>());
							 
							// Loop thought front balls to smoothly move it to position.
							for (int k = insertIndex; k >= 0; k--)
							{
								balls[k]->progression += ballLengthInSegment;
								balls[k]->MoveToDestination(balls[k]->transform->GetPosition(), 0.5f);
								if (balls[k]->connectToFront == false)
								{
									// Check for reconnection
									if (k > 0)
									{
										if (balls[k - 1]->progression - balls[k]->progression < ballLengthInSegment)
										{
											balls[k]->progression = balls[k - 1]->progression - ballLengthInSegment;
											balls[k]->connectToFront = true;
										}
									}
									break;
								}
							}

							if (insertIndex <= currentHeadIndex)
								currentHeadIndex++;

							i++;
							break;
						}
					}
				}

				if (balls[i]->CheckEndOfAnimation() && balls[i]->checkMatch && balls[i]->isChecked == false)
				{
					balls[i]->checkMatch = false;
					CheckNeighbor(i, balls[i]->ballColor);
				}

			}

		}

		if (matchBalls.size() >= 3)
		{
			int lastIndex = balls.size() - 1;

			for (size_t i = 0; i < matchBalls.size(); i++)
			{
				int removeIndex = RemoveBall(matchBalls[i]->gameObject->GetUniqueID());
				if (lastIndex > removeIndex)
				{
					lastIndex = removeIndex;
				}
			}

			if (currentHeadIndex < lastIndex || currentHeadIndex == 0)
			{
				currentHeadIndex = lastIndex;
			}


			balls[lastIndex]->connectToFront = false;
		}

		matchBalls.clear();
	}

	void BallPathController::CheckNeighbor(int checkIndex, BallColor checkColor)
	{
		balls[checkIndex]->isChecked = true;
		matchBalls.push_back(balls[checkIndex]);
		// Check Left
		if (checkIndex - 1 >= 0)
		{
			if (balls[checkIndex - 1]->ballColor == checkColor && balls[checkIndex - 1]->enabled == true && balls[checkIndex - 1]->isChecked == false && balls[checkIndex]->connectToFront == true)
			{
				CheckNeighbor(checkIndex - 1, checkColor);
			}
		}

		// Check Right
		if (checkIndex + 1 < balls.size())
		{
			if (balls[checkIndex + 1]->ballColor == checkColor && balls[checkIndex + 1]->enabled == true && balls[checkIndex + 1]->isChecked == false && balls[checkIndex + 1]->connectToFront == true)
			{
				CheckNeighbor(checkIndex + 1, checkColor);
			}
		}
	}

	int BallPathController::RemoveBall(int uniqueID)
	{
		for (size_t i = 0; i < balls.size(); i++)
		{
			if (balls[i]->gameObject->GetUniqueID() == uniqueID)
			{
				balls[i]->gameObject->enabled = false;
				balls.erase(balls.begin() + i);
				return i;
			}
		}
	}
}

