#pragma once
#include "MonoBehaviour.h"

#include "Input.h"

namespace DucklingsEngine
{
	class DuckController : public MonoBehaviour
	{
	public:
		DuckController() { Start(); }
		~DuckController() = default;

		void Start() override;
		void Update() override;
	};

	void DuckController::Start()
	{
		// This will execute once at the begginning of the scene.
		this->transform->SetPosition(Vector2(0.0f, 540.0f));
	}

	void DuckController::Update()
	{
		// This will execute every frame in update.
		this->transform->Translate(Vector2(100 * Time::GetDeltaTime(), 0.0f));
		this->transform->localEulerAngles -= 10 * Time::GetDeltaTime();
	}
}

