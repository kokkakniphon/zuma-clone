#pragma once
#include "MonoBehaviour.h"

#include "SpriteRenderer.h"
#include "Input.h"
#include "Rigidbody.h"
#include "BallController.h"
#include "BallPathController.h"

namespace DucklingsEngine
{
	class FrogController : public MonoBehaviour
	{
	private:
		float lookAtMouseOffset = -90.0f;
		GameObject* frogTongue;
		GameObject* frogFace;
		BallColor currentBallColor;
		GameObject* currentBall;

		GameObject* nextBall;

		BallColor ballColor;
		BallColor nextBallColor;

		int currentShootBallNum = 0;

		float shootSpeed = 500;
		bool loadedScene = false;

	public:
		FrogController() = default;
		~FrogController() = default;

		void Start() override;
		void Update() override;

		void ShootBall(Vector2 dir);
		void ReloadBall();

		BallPathController *path;

		void SetBallColor(GameObject* target, BallColor color);
	};

	void FrogController::Start()
	{
		// This will execute once at the begginning of the scene.
		this->gameObject->AddComponent<SpriteRenderer>("SMALLFROGonPAD");

		frogTongue = new GameObject("FROG_tongue");
		frogTongue->transform->SetParent(this->transform);
		frogTongue->transform->localPosition = Vector2(0.0f, 10.0f);
		frogTongue->AddComponent<SpriteRenderer>("Tongue");

		currentBall = new GameObject("FROG_currentBall");
		currentBall->transform->SetParent(this->transform);
		currentBall->transform->localPosition = Vector2(0.0f, 20.0f);
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallBlue", true, 1, 47), std::to_string(currentBall->GetUniqueID()) + "Blue");
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallRed", true, 1, 50), std::to_string(currentBall->GetUniqueID()) + "Red");
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallGreen", true, 1, 50), std::to_string(currentBall->GetUniqueID()) + "Green");
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallYellow", true, 1, 50), std::to_string(currentBall->GetUniqueID()) + "Yellow");
		currentBall->AddComponent<SpriteRenderer>(); 

		nextBall = new GameObject("FROG_nextBall");
		nextBall->transform->SetParent(this->transform);
		nextBall->transform->localPosition = Vector2(0.0f, -25.0f);
		SpriteManager::GetInstance().AddSprite(new Sprite("baDotz", true, 6, 6), std::to_string(nextBall->GetUniqueID()));
		nextBall->AddComponent<SpriteRenderer>(SpriteManager::GetInstance().GetSprite(std::to_string(nextBall->GetUniqueID())));

		frogFace = new GameObject("FROG_face");
		frogFace->transform->SetParent(this->transform);
		frogFace->transform->localPosition = Vector2(0.0f, 0.0f);
		frogFace->AddComponent<SpriteRenderer>("SMALLFROGonPADonlyFACE");

		ReloadBall();
		ballColor = static_cast<BallColor>(std::rand() % 4);
		SetBallColor(currentBall, ballColor);
	}

	void FrogController::Update()
	{
		// This will execute every frame in update.

		if (path->started == false)
			return;

		// Rotate the frog toward mouse.
		Vector2Int mousePos = Input::GetMousePosition();
		Vector2 frogPos = this->transform->GetPosition();
		Vector2 frogToMouse = Vector2(mousePos.x - frogPos.x, mousePos.y - frogPos.y);
		this->transform->localEulerAngles = (atan2(frogToMouse.y, frogToMouse.x) * 180 / M_PI) + lookAtMouseOffset;

		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT))
		{
			ShootBall(frogToMouse);
		}

		if (Input::GetMouseButtonDown(SDL_BUTTON_RIGHT))
		{
			BallColor tempColor = ballColor;

			ballColor = nextBallColor;
			SetBallColor(currentBall, ballColor);

			nextBallColor = tempColor;
			SetBallColor(nextBall, nextBallColor);
		}

	}

	void FrogController::ShootBall(Vector2 dir)
	{
		GameObject *newBall = new GameObject("ShootBall_" + std::to_string(currentShootBallNum));
		newBall->tag = "ShootBall";
		newBall->transform->SetPosition(currentBall->transform->GetPosition());
		newBall->AddComponent<CircleCollider>().radius = 15.0f;
		newBall->AddComponent<Rigidbody>().velocity = (dir / dir.magnitude())* shootSpeed;
		newBall->GetComponent<Rigidbody>().gravityScale = 0.0f;
		newBall->GetComponent<Rigidbody>().drag = 0.0f;
		newBall->AddComponent<SpriteRenderer>();
		newBall->AddComponent<Animator>();
		newBall->AddComponent<BallController>().ballColor = ballColor;
		currentShootBallNum++;
		ReloadBall();
	}

	void FrogController::ReloadBall()
	{
		ballColor = nextBallColor;
		SetBallColor(currentBall, ballColor);

		nextBallColor = static_cast<BallColor>(std::rand() % 4);
		SetBallColor(nextBall, nextBallColor);
	}

	void FrogController::SetBallColor(GameObject* target, BallColor color)
	{

		switch (color)
		{
		case DucklingsEngine::Blue:
			target->GetComponent<SpriteRenderer>().sprite = SpriteManager::GetInstance().GetSprite(std::to_string(currentBall->GetUniqueID()) + "Blue");
			break;
		case DucklingsEngine::Green:
			target->GetComponent<SpriteRenderer>().sprite = SpriteManager::GetInstance().GetSprite(std::to_string(currentBall->GetUniqueID()) + "Green");
			break;
		case DucklingsEngine::Red:
			target->GetComponent<SpriteRenderer>().sprite = SpriteManager::GetInstance().GetSprite(std::to_string(currentBall->GetUniqueID()) + "Red");
			break;
		case DucklingsEngine::Yellow:
			target->GetComponent<SpriteRenderer>().sprite = SpriteManager::GetInstance().GetSprite(std::to_string(currentBall->GetUniqueID()) + "Yellow");
			break;
		default:
			break;
		}
	}
}

