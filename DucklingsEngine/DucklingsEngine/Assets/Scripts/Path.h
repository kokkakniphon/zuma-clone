#pragma once

#include "GameObject.h"
#include "CircleCollider.h"
#include "Vector2D.h"
namespace DucklingsEngine
{
	//
	// Summary: Most of this algorithm come from: https://www.youtube.com/watch?v=RF04Fi9OCPc&list=PLFt_AvWsXl0d8aDaovNztYf6iTChHzrHP
	//     I adapt the algorithm to work well with this engine.
	class Path
	{
	private:
		float offsetSize = 50.0f;

		int lastSeletedIndex = 0;
		GameObject* lastSelected = nullptr;
	public:
		std::vector<GameObject*> pointObjs;
		std::vector<Vector2> points;

		bool isClosed;
		bool autoSetControlPoints;

		Path(Vector2 centre, bool defaultPath = true)
		{
			isClosed = false;
			autoSetControlPoints = false;

			if (defaultPath == true)
			{
				for (int i = 0; i < 4; i++)
				{
					pointObjs.push_back(new GameObject("PathPoint"));
					pointObjs[i]->AddComponent<CircleCollider>().radius = 5.0f;
					pointObjs[i]->GetComponent<CircleCollider>().showOutline = false;
					pointObjs[i]->GetComponent<CircleCollider>().isTrigger = true;

					points.push_back(pointObjs[i]->transform->GetPosition());
				}
				pointObjs[0]->transform->SetPosition(Vector2(centre + Vector2::left() * offsetSize));
				pointObjs[1]->transform->SetPosition(Vector2(centre + (Vector2::left() * offsetSize + Vector2::up() * offsetSize) * 0.5f));
				pointObjs[2]->transform->SetPosition(Vector2(centre + (Vector2::right() * offsetSize + Vector2::down() * offsetSize) * 0.5f));
				pointObjs[3]->transform->SetPosition(Vector2(centre + Vector2::right() * offsetSize));

				UpdateAllPoints();
			}
		}

		Path(std::vector<Vector2> points)
		{
			isClosed = false;
			autoSetControlPoints = false;

			for (int i = 0; i < points.size(); i++)
			{
				pointObjs.push_back(new GameObject("PathPoint"));
				pointObjs[i]->AddComponent<CircleCollider>().radius = 5.0f;
				pointObjs[i]->GetComponent<CircleCollider>().showOutline = false;
				pointObjs[i]->GetComponent<CircleCollider>().isTrigger = true;
				pointObjs[i]->transform->SetPosition(points[i]);

				this->points.push_back(pointObjs[i]->transform->GetPosition());
			}
			pointObjs[3]->GetComponent<CircleCollider>().isTrigger = false;

			UpdateAllPoints();
		}

		~Path() = default;

		int NumPoints()
		{
			return pointObjs.size();
		}

		int NumSegments()
		{
			return pointObjs.size() / 3;
		}

		void AddSegment(Vector2 anchorPos)
		{
			int size = pointObjs.size();

			pointObjs.push_back(new GameObject("PathPoint"));
			size = pointObjs.size();
			pointObjs[size - 1]->AddComponent<CircleCollider>().radius = 5.0f;
			pointObjs[size - 1]->GetComponent<CircleCollider>().showOutline = false;
			pointObjs[size - 1]->GetComponent<CircleCollider>().isTrigger = true;
			pointObjs[size - 1]->transform->SetPosition(pointObjs[size - 2]->transform->GetPosition() * 2 - pointObjs[size - 3]->transform->GetPosition());
			
			points.push_back(pointObjs[size - 1]->transform->GetPosition());

			pointObjs.push_back(new GameObject("PathPoint"));
			size = pointObjs.size();
			pointObjs[size - 1]->AddComponent<CircleCollider>().radius = 5.0f;
			pointObjs[size - 1]->GetComponent<CircleCollider>().showOutline = false;
			pointObjs[size - 1]->GetComponent<CircleCollider>().isTrigger = true;
			pointObjs[size - 1]->transform->SetPosition((pointObjs[size - 2]->transform->GetPosition() + anchorPos) * 0.5f);

			points.push_back(pointObjs[size - 1]->transform->GetPosition());

			pointObjs.push_back(new GameObject("PathPoint"));
			size = pointObjs.size();
			pointObjs[size - 1]->AddComponent<CircleCollider>().radius = 5.0f;
			pointObjs[size - 1]->GetComponent<CircleCollider>().showOutline = false;
			pointObjs[size - 1]->GetComponent<CircleCollider>().isTrigger = true;
			pointObjs[size - 1]->transform->SetPosition(anchorPos);

			points.push_back(pointObjs[size - 1]->transform->GetPosition());

			if (autoSetControlPoints)
			{
				AutoSetAllAffectedControlPoints(pointObjs.size() - 1);
			}
		}

		void DeleteSegment(int id)
		{
			if (NumSegments() > 2 || isClosed == false && NumSegments() > 1)
			{
				int anchorIndex;
				for (int i = 0; i < pointObjs.size(); i++)
				{
					if (pointObjs[i]->GetUniqueID() == id)
					{
						anchorIndex = i;
						break;
					}
				}

				if (anchorIndex % 3 == 0)
				{
					if (anchorIndex == 0)
					{
						if (isClosed)
						{
							pointObjs[pointObjs.size() - 1] = pointObjs[2];
							points[pointObjs.size() - 1] = points[2];
						}
						for (int i = 0; i < 3; i++)
						{
							pointObjs[0]->Destroy();
							pointObjs.erase(pointObjs.begin());
							points.erase(points.begin());
						}
					}
					else if (anchorIndex == pointObjs.size() - 1 && isClosed == false)
					{
						for (int i = 0; i < 3; i++)
						{
							pointObjs[pointObjs.size() - 1]->Destroy();
							pointObjs.pop_back();
							points.pop_back();
						}
					}
					else
					{
						for (int i = -1; i < 2; i++)
						{
							pointObjs[anchorIndex + i]->Destroy();
							pointObjs.erase(pointObjs.begin() + anchorIndex + i);
							points.pop_back();
						}
					}
				}
			}
		}

		std::vector<Vector2> GetPointsInSegment(int i)
		{
			return std::vector<Vector2> { points[i * 3], points[i * 3 + 1], points[i * 3 + 2], points[LoopIndex(i * 3 + 3)] };
		}

		void MovePoint(int id, Vector2 pos)
		{
			int selectedIndex = 0;
			GameObject* selected = nullptr;
			if (lastSelected != nullptr && lastSelected->GetUniqueID() == id)
			{
				selectedIndex = lastSeletedIndex;
				selected = lastSelected;
			}
			else
			{
				for (int i = 0; i < pointObjs.size(); i++)
				{
					if (pointObjs[i]->GetUniqueID() == id)
					{
						selectedIndex = i;
						selected = pointObjs[i];
						break;
					}
				}
			}

			if (selected != nullptr)
			{
				Vector2 deltaMove = pos - selected->transform->GetPosition();

				if (selectedIndex % 3 == 0 || !autoSetControlPoints)
				{
					selected->transform->SetPosition(pos);
					points[selectedIndex] = selected->transform->GetPosition();

					if (autoSetControlPoints)
					{
						AutoSetAllAffectedControlPoints(selectedIndex);
					}
					else
					{
						if (selectedIndex % 3 == 0)
						{
							if (selectedIndex + 1 < pointObjs.size() || isClosed)
							{
								pointObjs[LoopIndex(selectedIndex + 1)]->transform->Translate(deltaMove);
								points[LoopIndex(selectedIndex + 1)] = pointObjs[LoopIndex(selectedIndex + 1)]->transform->GetPosition();
							}

							if (selectedIndex - 1 >= 0 || isClosed)
							{
								pointObjs[LoopIndex(selectedIndex - 1)]->transform->Translate(deltaMove);
								points[LoopIndex(selectedIndex - 1)] = pointObjs[LoopIndex(selectedIndex - 1)]->transform->GetPosition();
							}
						}
						else
						{
							bool nextPointIsAncher = ((selectedIndex + 1) % 3 == 0);
							int correspondingControlIndex = (nextPointIsAncher) ? selectedIndex + 2 : selectedIndex - 2;
							int ancherIndex = (nextPointIsAncher) ? selectedIndex + 1 : selectedIndex - 1;

							if (correspondingControlIndex >= 0 && correspondingControlIndex < pointObjs.size() || isClosed)
							{
								Vector2 ancherPoint = points[LoopIndex(ancherIndex)];
								Vector2 correspondingPoint = points[LoopIndex(correspondingControlIndex)];
								float dst = Vector2::Distance(ancherPoint, points[selectedIndex]);
								float dirDis = sqrtf(std::pow(ancherPoint.x - pos.x, 2) + std::pow(ancherPoint.y - pos.y, 2));
								Vector2 dir = Vector2((ancherPoint.x - pos.x) / dirDis, (ancherPoint.y - pos.y) / dirDis);
								pointObjs[LoopIndex(correspondingControlIndex)]->transform->SetPosition(ancherPoint + dir * dst);
								points[LoopIndex(correspondingControlIndex)] = pointObjs[LoopIndex(correspondingControlIndex)]->transform->GetPosition();
							}
						}
					}
				}

				lastSeletedIndex = selectedIndex;
				lastSelected = selected;
			}
		}

		void ToggleAutoSetControlPoints()
		{
			autoSetControlPoints = !autoSetControlPoints;
			if (autoSetControlPoints)
			{
				AutoSetAllControlPoints();
			}
		}

		void ToggleClosed()
		{
			isClosed = !isClosed;

			if (isClosed)
			{
				int size = pointObjs.size();

				pointObjs.push_back(new GameObject("PathPoint"));
				size = pointObjs.size();
				pointObjs[size - 1]->AddComponent<CircleCollider>().radius = 5.0f;
				pointObjs[size - 1]->GetComponent<CircleCollider>().showOutline = false;
				pointObjs[size - 1]->GetComponent<CircleCollider>().isTrigger = true;
				pointObjs[size - 1]->transform->SetPosition(points[size - 2] * 2 - points[size - 3]);

				points.push_back(pointObjs[size - 1]->transform->GetPosition());

				pointObjs.push_back(new GameObject("PathPoint"));
				size = pointObjs.size();
				pointObjs[size - 1]->AddComponent<CircleCollider>().radius = 5.0f;
				pointObjs[size - 1]->GetComponent<CircleCollider>().showOutline = false;
				pointObjs[size - 1]->GetComponent<CircleCollider>().isTrigger = true;
				pointObjs[size - 1]->transform->SetPosition(points[0] * 2 - points[1]);

				points.push_back(pointObjs[size - 1]->transform->GetPosition());

				if (autoSetControlPoints)
				{
					AutoSetAnchorControlPoints(0);
					AutoSetAnchorControlPoints(pointObjs.size() - 3);
				}
			}
			else
			{
				pointObjs[pointObjs.size() - 1]->Destroy();
				pointObjs.pop_back();

				points.pop_back();
				
				pointObjs[pointObjs.size() - 1]->Destroy();
				pointObjs.pop_back();

				points.pop_back();

				if (autoSetControlPoints)
				{
					AutoSetStartAndEndControls();
				}
			}
		}

		void AutoSetAllAffectedControlPoints(int updatedAnchorIndex)
		{
			for (unsigned int i = updatedAnchorIndex - 3; i <= updatedAnchorIndex + 3; i+=3)
			{
				if (i >= 0 && i < pointObjs.size() || isClosed)
				{
					AutoSetAnchorControlPoints(LoopIndex(i));
				}
			}

			AutoSetStartAndEndControls();
		}

		void AutoSetAllControlPoints()
		{
			for (unsigned int i = 0; i < pointObjs.size(); i+=3)
			{
				AutoSetAnchorControlPoints(i);
			}

			AutoSetStartAndEndControls();
		}

		void AutoSetAnchorControlPoints(int anchorIndex)
		{
			Vector2 anchorPos = points[anchorIndex];
			Vector2 dir = Vector2();
			float neighbourDistances[2] = {0.0f, 0.0f};

			if (anchorIndex - 3 >= 0 || isClosed)
			{
				Vector2 offset = points[LoopIndex(anchorIndex - 3)] - anchorPos;
				float offsetDis = offset.magnitude();
				dir += offset / offsetDis;
				neighbourDistances[0] = offsetDis;
			}
			if (anchorIndex + 3 >= 0 || isClosed)
			{
				Vector2 offset = points[LoopIndex(anchorIndex + 3)] - anchorPos;
				float offsetDis = offset.magnitude();
				dir -= (offset / offsetDis);
				neighbourDistances[1] = -offsetDis;
			}
			
			float dirDis = dir.magnitude();
			dir = dir / dirDis;

			for (unsigned int i = 0; i < 2; i++)
			{
				int controlIndex = anchorIndex + i * 2 - 1;
				if (controlIndex >= 0 && controlIndex < pointObjs.size() || isClosed)
				{
					pointObjs[LoopIndex(controlIndex)]->transform->SetPosition(Vector2(anchorPos.x + dir.x * neighbourDistances[i] * 0.5f, anchorPos.y + dir.y * neighbourDistances[i] * 0.5f));
					points[LoopIndex(controlIndex)] = pointObjs[LoopIndex(controlIndex)]->transform->GetPosition();
				}
			}
		}

		void AutoSetStartAndEndControls()
		{
			if (isClosed == false)
			{
				pointObjs[1]->transform->SetPosition((points[0] + points[2]) * 0.5f);
				points[1] = pointObjs[1]->transform->GetPosition();
				pointObjs[pointObjs.size()-2]->transform->SetPosition((points[pointObjs.size() - 1] + points[pointObjs.size() - 3]) * 0.5f);
				points[pointObjs.size() - 2] = pointObjs[pointObjs.size() - 2]->transform->GetPosition();
			}
		}

		int LoopIndex(int i)
		{
			return (i + pointObjs.size()) % pointObjs.size();
		}

		//  ref: https://hal.inria.fr/file/index/docid/518379/filename/Xiao-DiaoChen2007c.pdf
		// Summary:
		//     Get point projection for path curve.
		Vector2 ClosetPointOnPath()
		{
			// Will implement if needed.
		}

		void UpdateAllPoints()
		{
			for (int i = 0; i < pointObjs.size(); i++)
			{
				points[i] = pointObjs[i]->transform->GetPosition();
			}
		}
	};
}

