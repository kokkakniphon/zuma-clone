#pragma once

#include "MonoBehaviour.h"

#include "Input.h"

namespace DucklingsEngine
{
	class PenguinController : public MonoBehaviour
	{
	public:
		bool addedForce = false;
		bool isLoaded = false;
		float loadSceneCountDown = 5.0f;

		PenguinController() {}
		~PenguinController() = default;

		void Start() override;
		void Update() override;
	};

	void PenguinController::Start()
	{
		//this->gameObject->GetComponent<SpriteRenderer>().enabled = false;
		this->transform->localScale = Vector2(0.5f, 0.5f);
	}

	void PenguinController::Update()
	{
		transform->localEulerAngles += 10 * Time::GetDeltaTime();

		if(loadSceneCountDown > 0)
			loadSceneCountDown -= Time::GetDeltaTime();

		if (loadSceneCountDown < 0 && isLoaded == false)
		{
			this->gameObject->GetComponent<SpriteRenderer>().enabled = true;
			loadSceneCountDown = 0;
			isLoaded = true;
		}

		if (this->transform->GetPosition().y > 1080 && addedForce == false)
		{
			addedForce = true;
			this->gameObject->GetComponent<Rigidbody>().velocity = Vector2::zero();
			this->gameObject->GetComponent<Rigidbody>().AddForce(Vector2(0, -300));
		}

		if (this->transform->GetPosition().y < 1000 && addedForce == true)
		{
			addedForce = false;
		}

		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT) == true)
		{
			SceneManager::GetInstance().LoadScene(1);
			std::cout << "Left Mouse Button Down" << std::endl;
		}

		if (Input::GetMouseButton(SDL_BUTTON_LEFT) == true)
		{
			std::cout << "Left Mouse Button" << std::endl;
		}

		if (Input::GetMouseButtonUp(SDL_BUTTON_LEFT) == true)
		{
			std::cout << "Left Mouse Button Up" << std::endl;
		}

		if (Input::GetMouseButtonDown(SDL_BUTTON_RIGHT) == true)
		{
			SceneManager::GetInstance().UnloadScene(1);
			std::cout << "Right Mouse Button Down" << std::endl;
		}

		if (Input::GetMouseButton(SDL_BUTTON_RIGHT) == true)
		{
			std::cout << "Right Mouse Button" << std::endl;
		}

		if (Input::GetMouseButtonUp(SDL_BUTTON_RIGHT) == true)
		{
			std::cout << "Right Mouse Button Up" << std::endl;
		}


		if (Input::GetKeyDown(SDL_SCANCODE_H) == true)
		{
			this->transform->SetPosition(Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y));
			this->gameObject->GetComponent<Rigidbody>().velocity = Vector2();
			std::cout << "H Key Down" << std::endl;
		}

		if (Input::GetKey(SDL_SCANCODE_H) == true)
		{
			std::cout << "H Key" << std::endl;
		}

		if (Input::GetKeyUp(SDL_SCANCODE_H) == true)
		{
			std::cout << "H Key Up" << std::endl;
		}

		if (Input::GetKeyDown(SDL_SCANCODE_J) == true)
		{
			std::cout << "J Key Down" << std::endl;
		}

		if (Input::GetKey(SDL_SCANCODE_J) == true)
		{
			std::cout << "J Key" << std::endl;
		}

		if (Input::GetKeyUp(SDL_SCANCODE_J) == true)
		{
			std::cout << "J Key Up" << std::endl;
		}
	}
	
}

