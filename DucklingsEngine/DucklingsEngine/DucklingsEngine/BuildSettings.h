#pragma once

#include "SceneManager.h"

//
// Summary:
//     Include a scene here.
//     Example: #include "{scene_name}.h";
#include "Scenes/Zuma.h"

namespace DucklingsEngine
{
	class Settings
	{
	public:
		static void SetupBuildSettings()
		{
			//
			// Summary:
			//     Add scene to build settings list of scene.
			//     Example: SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared<{scene_name}>()); 
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <Zuma>());

			SceneManager::GetInstance().Init();
		}
	};
}

