#include "Collider.h"

#include "ColliderManager.h"

bool DucklingsEngine::Collider::Init()
{
	transform = &gameObject->GetComponent<Transform>();

	if(&gameObject->GetComponent<Rigidbody>() != nullptr)
		std::cout << "Attach Rigidbody to " << this->gameObject->name << std::endl;
	attachedRigidbody = &gameObject->GetComponent<Rigidbody>();

	ColliderManager::GetInstance().AddCollider(this);

	return true;
}
