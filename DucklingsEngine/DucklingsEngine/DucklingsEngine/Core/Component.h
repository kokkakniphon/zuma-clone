#pragma once

class Object;

#include <iostream>

namespace DucklingsEngine
{
	class Component
	{
	public:
		Component() = default;
		virtual ~Component() = default;

		//
		// Summary:
		//     Enabled Component are Updated, disabled Component are not.
		bool enabled = true;

		Object* gameObject;

		virtual bool Init() { return true; }
		virtual void Start() = 0 {}
		virtual void Draw() = 0 {}
		virtual void Update() = 0 {}
		virtual void FixedUpdate() = 0 {}
	};
}