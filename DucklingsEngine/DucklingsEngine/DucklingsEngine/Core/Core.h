#pragma once

#ifdef DE_PLATFORM_WINDOWS
	#ifdef DE_BUILD_DLL
		#define DUCKLINGS_ENGINE_API _declspec(dllexport)
	#else
		#define DUCKLINGS_ENGINE_API _declspec(dllimport)
	#endif // DE_BUILD_DLL
#else
	#error DucklingsEngine only support Windows!
#endif // DE_PLATFORM_WINDOWS