#include "Engine.h"

#include "BuildSettings.h"
#include "Input.h"

namespace DucklingsEngine
{
	SDL_Event Engine::event;
	Engine* Engine::s_Instance = nullptr;

	Engine::Engine() 
	{
		m_IsRunning = false;
		m_Window = nullptr;
		m_Renderer = nullptr;

		Time::GetInstance().timeScale = 1.0f;

		m_lastTimeScale = Time::GetInstance().timeScale;
	}

	Engine::~Engine() {}


	//
	// Summary:
	//		Order of Execution loop
	//		- Physics
	//		- Input events
	//		- Game logics
	//		- Scene rendering
	//
	void Engine::Run()
	{
		Init();

		Settings::SetupBuildSettings();
		
		Input::Init();

		while (IsRunning())
		{
			Time::GetInstance().Tick(PHYSIC_LOOP_CALULATION);
			for (unsigned int i = 0; i < PHYSIC_LOOP_CALULATION; i++)
			{
				ColliderManager::GetInstance().FixedUpdate();
				FixedUpdate();
			}
			if(SDL_PollEvent(&event) != 0)
				Input::Listen();
			Input::Update();
			Events();
			Update();
			Render();
			Input::Clear();
			frameCount++;
			currentFrame += Time::GetDeltaTime();
			if (currentFrame >= 1.0f)
			{
				std::cout << "FPS: " << frameCount << std::endl;
				currentFrame = 0;
				frameCount = 0;
			}
		}

		Clean();
	}

	bool Engine::Init()
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0 && IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) != 0) // If fail initializing SDL return false
		{
			SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
			return false;
		}

		auto wflags = (SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
		m_Window = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, wflags);

		if (m_Window == nullptr)
		{
			SDL_Log("Failed to create Window: %s", SDL_GetError());
			return false;
		}


		m_Renderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (m_Renderer == nullptr)
		{
			SDL_Log("Failed to create Renderer: %s", SDL_GetError());
			return false;
		}

		manager = new EntityManager();

		m_ClearColor = DARK;


		return m_IsRunning = true;
	}

	void Engine::Update()
	{
		manager->Start();
		manager->Update();
	}

	void Engine::FixedUpdate()
	{
		manager->FixedUpdate();
	}

	void Engine::Render()
	{
		SDL_SetRenderDrawColor(m_Renderer, m_ClearColor.r, m_ClearColor.g, m_ClearColor.b, m_ClearColor.a);
		SDL_RenderClear(m_Renderer);

		manager->Draw();
		ColliderManager::GetInstance().OnDrawGizmos(DRAW_GIZMOS);

		SDL_RenderPresent(m_Renderer);
	}

	void Engine::Events()
	{
		switch (event.type)
		{
		case SDL_QUIT:
			Engine::GetInstance().Quit();
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_ENTER:
				windowEnter = true;
				Time::GetInstance().timeScale = Engine::GetInstance().m_lastTimeScale;
				break;
			case SDL_WINDOWEVENT_LEAVE:
				if (windowEnter == true)
				{
					windowEnter = false;
					Engine::GetInstance().m_lastTimeScale = Time::GetInstance().timeScale;
					Time::GetInstance().timeScale = 0.0f;
				}
				break;
			}
			break;
		}
	}

	void Engine::Clean()
	{
		AssetManager::GetInstance().Clean();
		SceneManager::GetInstance().Clean();
		Time::GetInstance().Clean();
		ColliderManager::GetInstance().Clean();
		SpriteManager::GetInstance().Clean();

		SDL_DestroyRenderer(m_Renderer);
		SDL_DestroyWindow(m_Window);
		SDL_Quit();

		delete manager;
		delete s_Instance;
	}

	void Engine::Quit()
	{
		m_IsRunning = false;
	}
}
