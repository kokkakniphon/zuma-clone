#pragma once

#include "Core.h"

#define SDL_MAIN_HANDLED

#include "SDL.h"
#include "SDL_image.h"

#include "Time.h"
#include "SceneManager.h"
#include "EntityManager.h"
#include "AssetManager.h"
#include "ColliderManager.h"
#include "SpriteManager.h"

#include <iostream>


constexpr int SCREEN_WIDTH = 640;
constexpr int SCREEN_HEIGHT = 480;
constexpr SDL_Color DARK = {30, 30, 30, 255};

constexpr int PHYSIC_LOOP_CALULATION = 1;

constexpr bool DRAW_GIZMOS = false;

namespace DucklingsEngine
{
	class DUCKLINGS_ENGINE_API Engine
	{
	private:

		bool m_IsRunning;

		SDL_Color m_ClearColor;
		SDL_Window* m_Window;
		static Engine* s_Instance;

		int frameCount = 0;
		float fps = 0, currentFrame = 0;

		const char* windowTitle = "ZUMA - by Niphon";

		bool Init();
		void Clean();

		void Update();
		void FixedUpdate();
		void Render();
		void Events();

	public:
		SDL_Renderer* m_Renderer;

		EntityManager* manager;

		Engine();
		~Engine();

		inline static Engine& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new Engine();
			}
			return *s_Instance;
		}

		void Run();
		void Quit();

		float m_lastTimeScale;
		static SDL_Event event;

		bool windowEnter = true;

		inline bool IsRunning() { return m_IsRunning; }
		inline SDL_Renderer* GetRenderer() { return m_Renderer; }
	};
}
