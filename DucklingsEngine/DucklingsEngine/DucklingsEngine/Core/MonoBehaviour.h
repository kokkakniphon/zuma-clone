#pragma once

#include "Component.h"

namespace DucklingsEngine
{
	class MonoBehaviour : public Component
	{
	public:
		Transform* transform = nullptr;

		bool Init() override { transform = &gameObject->GetComponent<Transform>(); return true; }
		void Draw() {}
		void FixedUpdate() {}
	};
}