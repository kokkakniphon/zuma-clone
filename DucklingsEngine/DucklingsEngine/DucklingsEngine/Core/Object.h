#pragma once

#include "ECS.h"
#include <string>
#include <vector>
#include <memory>
#include "Component.h"
#include "Transform.h"

#include <iostream>

namespace DucklingsEngine
{
	//
	//	Summary:
	//		Base class for all objects DucklingEngine can reference.
	class Object
	{
	private:
		unsigned int uniqueID;

		static unsigned int IDseed;
		static int GenerateUniqueID() { IDseed++; return IDseed; }
	public:

		//
		// Summary:
		//     The name of the object.
		std::string name;
		//
		// Summary:
		//     The tag of this game object.
		std::string tag;

		//
		// Summary:
		//     Enabled Component are Updated, disabled Component are not.
		bool enabled = true;

		Object() { this->AddComponent<Transform>(); uniqueID = GenerateUniqueID(); m_Active = true; enabled = true; }
		virtual ~Object() = default;

		unsigned int GetUniqueID() { return uniqueID; }

		//
		// Summary:
		//     Add the component of Type type to the object.
		//
		// Parameters:
		//   type:
		//     The type of Component to dispatch.
		template<typename T, typename... TArgs>
		inline T& AddComponent(TArgs&&... args)
		{
			T* comp(new T(std::forward<TArgs>(args)...));
			std::unique_ptr<Component> uptr{ comp };
			m_Components.emplace_back(std::move(uptr));

			comp->gameObject = this;
			comp->enabled = true;

			if (comp->Init())
			{
				m_ComponentList[getComponentTypeID<T>()] = comp;
				m_ComponentBitset[getComponentTypeID<T>()] = true;
				return *comp;
			}
			else
			{
				comp->gameObject = nullptr;
			}

			return *static_cast<T*>(nullptr);
		}

		//
		// Summary:
		//     Returns the component of Type type if the object has one attached, null
		//     if it doesn't.
		//
		// Parameters:
		//   type:
		//     The type of Component to retrieve.
		template<typename T>
		inline T& GetComponent() const
		{
			auto ptr(m_ComponentList[getComponentTypeID<T>()]);
			return *static_cast<T*>(ptr);
		}

		//
		// Summary:
		//     Return True if the object has the component of Type type attached, false
		//		if it doesn't
		template<typename T>
		inline bool HasComponent() const { return m_ComponentBitset[getComponentTypeID<T>()]; }

		//
		// Summary:
		//     Return True if the object is in active state, false
		//		if it doesn't
		inline bool isActive() const { return m_Active; }

		inline void Destroy() { m_Active = false; }

		inline void Draw() { for (auto& component : m_Components) { if (component->enabled == true) { component->Draw(); } } }

		inline void Start() { for (auto& component : m_Components) { component->Start(); } }

		inline void Update() { for (int i = m_Components.size() - 1; i >= 0; i--) { if (m_Components[i]->enabled == true) { m_Components[i]->Update(); } } }

		inline void FixedUpdate() { for (int i = m_Components.size() - 1; i >= 0; i--) { if (m_Components[i]->enabled == true) { m_Components[i]->FixedUpdate(); } } }

	private:
		bool m_Active = true;
		ComponentList m_ComponentList;
		ComponentBitset m_ComponentBitset;
		std::vector<std::unique_ptr<Component>> m_Components;
	};

}