#pragma once

#include <string>
#include "Component.h"

namespace DucklingsEngine
{
	class Renderer : public Component
	{
	public:

		//
		// Summary:
		//     Makes the rendered 3D object visible if enabled.
		bool enabled;
		//
		// Summary:
		//     This value sorts renderers by priority. Lower values are rendered first and higher
		//     values are rendered last.
		int rendererPriority;
		//
		// Summary:
		//     Name of the Renderer's sorting layer.
		std::string sortingLayerName;
		//
		// Summary:
		//     Unique ID of the Renderer's sorting layer.
		int sortingLayerID;
		//
		// Summary:
		//     Renderer's order within a sorting layer.
		int sortingOrder;

	};
}