#include "Time.h"

namespace DucklingsEngine
{
	Time* Time::s_Instance = nullptr;

	Time::Time()
	{
		this->timeScale = 1.0f;
		this->unscaledTime = 0.0f;
		this->time = 0.0f;
		this->deltaTime = 0.0f;
		this->oldTime = SDL_GetTicks();
	}

	void Time::Tick(int fixedTimeDivision)
	{
		newTime = SDL_GetTicks();

		this->deltaTime = ((float)(newTime - oldTime) / 1000.0f);

		if (this->deltaTime > 1.0f / MINIMUM_FPS)
			this->deltaTime = 1.0f / MINIMUM_FPS;

		this->time += this->deltaTime * timeScale;
		this->unscaledTime += this->deltaTime;
		this->fixedDeltaTime = this->deltaTime / (float)fixedTimeDivision;

		oldTime = newTime;
	}
	void Time::Clean()
	{
		if(s_Instance != nullptr)
			delete s_Instance;
	}
}