#pragma once

#include "EdgeCollider.h"

constexpr float deg2rad = M_PI / 180;
constexpr float rad2deg = 180 / M_PI;

namespace DucklingsEngine
{
	class BoxCollider : public EdgeCollider
	{
	public:

		//
		// Summary:
		//     The width and height of the rectangle.
		Vector2 size = Vector2::ones();

		//
		// Summary:
		//     Check if a collider overlaps a point in space.
		//
		// Parameters:
		//   point:
		//     A point in world space.
		//
		// Returns:
		//     Does point overlap the collider?
		bool OverlapPoint(Vector2 point) override;
		//
		// Summary:
		//     Get a list of all colliders that overlap this collider.
		//
		// Parameters:
		//
		//   collider:
		//     The target collider to calculate the overlaping.
		//
		// Returns:
		//     Does this collider overlap the target collider?
		bool OverlapCollider(Collider* collider) override;

		void StaticResolution(Collider* collider) override;
		void DynamicResolution(Collider* collider) override;

		void Start();
		void Update();
		void FixedUpdate();
		void Draw() {}
	};
}

