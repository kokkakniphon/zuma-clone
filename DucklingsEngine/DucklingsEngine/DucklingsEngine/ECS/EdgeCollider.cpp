#include "EdgeCollider.h"

#include "Engine.h"

namespace DucklingsEngine
{
	Vector2 EdgeCollider::ClosestPoint(Vector2 position)
	{
		Vector2 sourcePos = this->transform->GetPosition();

		Vector2 closestPoint;

		if(points.size() > 0)
			closestPoint = points[0] + sourcePos;

		float closestDis;
		
		for (unsigned int i = 0; i < points.size() - 1; i++)
		{
			Vector2 p1 = points[i] + sourcePos;
			Vector2 p2 = points[i + 1] + sourcePos;

			Vector2 u = position - p1;
			Vector2 v = p2 - p1;

			float lineDis = sqrtf(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));

			float t = std::max(0.0f, std::min(1.0f, Vector2::Dot(u, v) / pow(lineDis, 2)));

			Vector2 l_closestPoint = p1 + v * t;
			float dis = sqrtf(pow(l_closestPoint.x - position.x, 2) + pow(l_closestPoint.y - position.y, 2));

			if (i == 0)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
			else if (dis < closestDis)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
		}

		if (closed == true && points.size() > 1)
		{
			Vector2 p1 = points[points.size()-1] + sourcePos;
			Vector2 p2 = points[0] + sourcePos;

			Vector2 u = position - p1;
			Vector2 v = p2 - p1;

			float lineDis = sqrtf(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));

			float t = std::max(0.0f, std::min(1.0f, Vector2::Dot(u, v) / pow(lineDis, 2)));

			Vector2 l_closestPoint = p1 + v * t;
			float dis = sqrtf(pow(l_closestPoint.x - position.x, 2) + pow(l_closestPoint.y - position.y, 2));

			if (dis < closestDis)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
		}
		return closestPoint;
	}

	void EdgeCollider::OnDrawGizmos()
	{
		if (showOutline == true)
		{
			Vector2 sourcePos = this->transform->GetPosition();

			for (unsigned int i = 0; i < points.size()-1; i++)
			{
				Vector2Int p1 = Vector2Int(points[i].x + sourcePos.x, points[i].y + sourcePos.y);
				Vector2Int p2 = Vector2Int(points[i+1].x + sourcePos.x, points[i+1].y + sourcePos.y);
				SDL_SetRenderDrawColor(Engine::GetInstance().m_Renderer, gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
				SDL_RenderDrawLine(Engine::GetInstance().m_Renderer, p1.x, p1.y, p2.x, p2.y);
			}

			if (closed == true && points.size() > 1)
			{
				Vector2Int p1 = Vector2Int(points[points.size()-1].x + sourcePos.x, points[points.size() - 1].y + sourcePos.y);
				Vector2Int p2 = Vector2Int(points[0].x + sourcePos.x, points[0].y + sourcePos.y);
				SDL_SetRenderDrawColor(Engine::GetInstance().m_Renderer, gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
				SDL_RenderDrawLine(Engine::GetInstance().m_Renderer, p1.x, p1.y, p2.x, p2.y);
			}

			gizmosColor = SDL_Color{ 0,255,0,255 };
		}
	}
}