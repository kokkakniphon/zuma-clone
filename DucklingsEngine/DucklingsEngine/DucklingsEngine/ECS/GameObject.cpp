#include "GameObject.h"
#include "Engine.h"

namespace DucklingsEngine
{
	GameObject::GameObject() : Object()
	{
		this->name = "GameObject";
		this->layer = 0;
		this->tag = "Untagged";
		this->transform = &this->GetComponent<Transform>();
		this->transform->name = this->name;

		Engine::GetInstance().manager->AddEntity(this);
	}

	GameObject::GameObject(std::string name) : Object()
	{
		this->name = name;
		this->layer = 0;
		this->tag = "Untagged";
		this->transform = &this->GetComponent<Transform>();
		this->transform->name = this->name;

		Engine::GetInstance().manager->AddEntity(this);
	}
}