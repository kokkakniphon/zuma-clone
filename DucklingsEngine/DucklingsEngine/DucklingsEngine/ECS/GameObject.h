#pragma once

#include "Object.h"

namespace DucklingsEngine
{
	class GameObject : public Object
	{
	private:

	public:
		
		//
		// Summary:
		//     The Transform attached to this GameObject.
		Transform* transform;
		//
		// Summary:
		//     The layer the game object is in.
		int layer;
		
		//
		// Summary:
		//     The local active state of this GameObject. (Read Only)
		bool activeSelf;

		//
		// Summary:
		//     Creates a new game object, named name.
		//
		// Parameters:
		//   name:
		//     The name that the GameObject is created with.
		//
		//   components:
		//     A list of Components to add to the GameObject on creation.
		GameObject();
		//
		// Summary:
		//     Creates a new game object, named name.
		//
		// Parameters:
		//   name:
		//     The name that the GameObject is created with.
		GameObject(std::string name);
		~GameObject() = default;
	};
}