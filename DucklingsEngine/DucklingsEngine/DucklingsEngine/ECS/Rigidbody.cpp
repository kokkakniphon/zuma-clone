#include "Rigidbody.h"

namespace DucklingsEngine
{
	Rigidbody::Rigidbody()
	{
		mass = UNI_MASS;
		gravityScale = GRAVITY;
	}

	bool Rigidbody::Init()
	{
		m_Transform = &gameObject->GetComponent<Transform>();
		return true;
	}

	void Rigidbody::Start() { }

	void Rigidbody::Update()
	{
		
	}

	void Rigidbody::FixedUpdate()
	{
		float fixedDeltaTime = Time::GetFixedDeltaTime();

		m_Accelaration = Vector2(-velocity.x * drag, -velocity.y * drag) / mass;
		m_Accelaration += Vector2(0, gravityScale * GRAVITY) / mass;
		velocity += m_Accelaration / drag * fixedDeltaTime;
		velocity.y = std::min(velocity.y, MAX_TERMINAL_VELOCITY);
		m_Transform->Translate(velocity * fixedDeltaTime);

		inertia = -angularVelocity * angularDrag;
		angularVelocity += inertia * fixedDeltaTime;
		m_Transform->localEulerAngles += angularVelocity * fixedDeltaTime;
	}

	void Rigidbody::AddTorque(float torque) // NEEDED to be implemented
	{
		
	}

	void Rigidbody::SetRotation(float angle)  // NEEDED to be implemented
	{

	}
}