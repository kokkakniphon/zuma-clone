#pragma once

#include "Time.h"
#include "Object.h"
#include "Vector2D.h"
#include <algorithm>

constexpr float UNI_MASS = 1.0f;
constexpr float GRAVITY = 9.8f * 2.0f;
constexpr float MAX_TERMINAL_VELOCITY = 500.0f;

namespace DucklingsEngine
{
	class Rigidbody : public Component
	{
	private:
		//
		// Summary:
		//     Linear velocity of the Rigidbody in units per second.
		Vector2 m_Accelaration = Vector2();

		Transform* m_Transform = nullptr;

	public:
		//
		// Summary:
		//     Mass of the Rigidbody.
		float mass = 1.0f;
		//
		// Summary:
		//     The degree to which this object is affected by gravity.
		float gravityScale = 1.0f;

		//
		// Summary:
		//     Linear velocity of the Rigidbody in units per second.
		Vector2 velocity = Vector2();
		
		//
		// Summary:
		//     Angular velocity in degrees per second.
		float angularVelocity = 0.0f;
		//
		// Summary:
		//     The rigidBody rotational inertia.
		float inertia = 0.0f;
		//
		// Summary:
		//     Coefficient of drag.
		float drag = 0.8f;
		//
		// Summary:
		//     Coefficient of angular drag.
		float angularDrag = 0.8f;


		Rigidbody();
		~Rigidbody() = default;

		bool Init() override;

		void Start() override;

		void Update() override;

		void FixedUpdate() override;

		void Draw() {}

		//
		// Summary:
		//     Apply a force to the rigidbody.
		//
		// Parameters:
		//   force:
		//     Components of the force in the X and Y axes.
		inline void AddForce(const Vector2 force) { velocity += force / mass; }
		//
		// Summary:
		//     Apply a torque at the rigidbody's centre of mass.
		//
		// Parameters:
		//   torque:
		//     Torque to apply.
		void AddTorque(float torque);
		//
		// Summary:
		//     Sets the rotation of the Rigidbody2D to angle (given in degrees).
		//
		// Parameters:
		//   angle:
		//     The rotation of the Rigidbody (in degrees).
		void SetRotation(float angle);
	};
}