#pragma once

#include "SDL.h"
#include "Sprite.h"
#include "Renderer.h"

#include <iostream>

namespace DucklingsEngine
{
	class SpriteRenderer : public Renderer
	{
	private:
		SDL_Rect m_DstRect = { 0, 0, 0, 0 };
		Transform* m_Transform = nullptr;
		SDL_RendererFlip m_Flip = SDL_FLIP_NONE;

		std::unique_ptr<Sprite> localSprite;

	public:
		//
		// Summary:
		//     The Sprite to render.
		//std::shared_ptr<Sprite> sprite;
		Sprite* sprite = nullptr;

		SpriteRenderer() = default;

		SpriteRenderer(std::string textureID)
		{
			localSprite = std::make_unique<Sprite>(textureID);
			sprite = localSprite.get();
		}
		SpriteRenderer(Sprite* sprite)
		{
			//this->sprite.reset(sprite);
			this->sprite = sprite;
		}
		~SpriteRenderer() = default;

		bool Init() override
		{
			m_Transform = &gameObject->GetComponent<Transform>();

			return true;
		}

		void Start() { }

		void Draw() override
		{
			if (sprite == nullptr)
				return;

			m_DstRect.w = static_cast<int>(sprite->rect.w * m_Transform->GetScale().x);
			m_DstRect.h = static_cast<int>(sprite->rect.h * m_Transform->GetScale().y);
			m_DstRect.x = static_cast<int>(m_Transform->GetPosition().x - m_DstRect.w * sprite->pivot.x);
			m_DstRect.y = static_cast<int>(m_Transform->GetPosition().y - m_DstRect.h * sprite->pivot.y);

			if (enabled == true)
			{
				if (sprite->isSpriteSheet == true)
				{
					sprite->rect.x = (sprite->GetCurrentSpriteCell() % sprite->spriteSheetColume) * sprite->rect.w;
					sprite->rect.y = (std::ceil((float)(sprite->GetCurrentSpriteCell() + 1) / (float)sprite->spriteSheetColume) - 1) * sprite->rect.h;
				}
		
				SDL_RenderCopyEx(Engine::GetInstance().m_Renderer, sprite->texture, &sprite->rect, &m_DstRect, m_Transform->GetEulerAngles(), nullptr, m_Flip);
			}
		}

		void Update() override
		{
			
		}

		void FixedUpdate() override
		{

		}
	};

}