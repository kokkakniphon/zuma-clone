#pragma once

#include "Vector2D.h"
#include "Component.h"

namespace DucklingsEngine
{
	class Transform : public Component
	{
	protected:
		//
		// Summary:
		//     The world space position of the Transform.
		Vector2 position = Vector2();
		//
		// Summary:
		//		The rotation of the transform relative to the world space.
		float eulerAngles = 0.0f;

	private:
		//
		// Summary:
		//     Returns the topmost transform in the hierarchy.
		Transform* root;
		//
		// Summary:
		//     The parent of the transform.
		Transform* parent;
		

	public:
		//
		// Summary:
		//     The world space position of the Transform.
		Vector2 localPosition = Vector2();
		//
		// Summary:
		//     The scale of the transform relative to the GameObjects parent.
		Vector2 localScale = Vector2(1.0f, 1.0f);
		//
		// Summary:
		//		The rotation of the transform relative to the local space.
		float localEulerAngles = 0.0f;

		//
		// Summary:
		//     The name of the object that have this transform.
		std::string name;
		
		//
		// Summary:
		//     Set the parent of the transform.
		//
		// Parameters:
		//   parent:
		//     The parent Transform to use.
		void SetParent(Transform* parent);
		//
		// Summary:
		//     Return parent of the transform.
		Transform* GetParent();

		//
		// Summary:
		//     Set global-position of the transform.
		void SetPosition(const Vector2& v);
		//
		// Summary:
		//     Return global-position of the transform.
		Vector2 GetPosition();
		//
		// Summary:
		//     Return global scale of the transform.
		Vector2 GetScale();
		//
		// Summary:
		//     Return global euler angles of the transform.
		float GetEulerAngles();

		//
		// Summary:
		//     Constructor of a new Transform with default component.
		Transform();
		virtual ~Transform() = default;

		//
		// Summary:
		//     Constructor of a new Transform with given local-position component.
		//
		// Parameters:
		//   v:
		Transform(Vector2 v);
		//
		// Summary:
		//     Constructor of a new Transform with given local-position, local-scale components.
		//
		// Parameters:
		//   v:
		//
		//   s:
		Transform(Vector2 v, Vector2 s);
		//
		// Summary:
		//     Constructor of a new Transform with given local-position, local-scale, rotation components.
		//
		// Parameters:
		//   v:
		//
		//   s:
		//
		//   r:
		Transform(Vector2 v, Vector2 s, float r);

		//
		// Summary:
		//     Moves the transform by x along the x axis, and y along the y axis.
		//
		// Parameters:
		//   x:
		//
		//   y:
		void Translate(float x, float y);
		//
		// Summary:
		//     Moves the transform in the direction and distance of translation.
		//
		// Parameters:
		//   translation:
		//
		//   relativeTo:
		void Translate(Vector2 v);

		bool Init() override { return true; }
		void Start() override {}
		void Update() override {}
		void FixedUpdate() override {}
		void Draw() override {}
	};
}