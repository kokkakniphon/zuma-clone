#pragma once

#include <iostream>
#include <vector>
#include <map>

#include "CircleCollider.h"

constexpr int PHYSIC_SUBDIVISION = 140;

namespace DucklingsEngine
{
	class ColliderManager
	{
	private:
		static ColliderManager* s_Instance;

		std::vector<Collider*> colliders;

		std::map<std::string, std::vector<Collider*>> segmentedColliders;

		std::vector<std::pair<Collider*, Collider*>> colliderPairs;

		bool OverlapCollider(Collider* source, Collider* target);
		void StaticResolution(Collider* source, Collider* target);
		void DynamicResolution(Collider* source, Collider* target);

	public:
		ColliderManager() = default;

		inline static ColliderManager& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new ColliderManager();
			}
			return *s_Instance;
		}

		void AddCollider(Collider* collider);
		bool OverlapPoint(std::vector<Collider*> *colliders, Vector2 point);
		void EraseCollider(Collider* collider);
		

		void FixedUpdate();
		void OnDrawGizmos(bool drawGizmos);
		void Clean();
	};
}
