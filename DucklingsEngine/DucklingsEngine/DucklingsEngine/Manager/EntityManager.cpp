#include "EntityManager.h"
#include <algorithm>
#include <iostream>

namespace DucklingsEngine
{
	void EntityManager::Draw()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->enabled == true)
			{
				m_Entities[i]->Draw();
			}
		}
	}

	void EntityManager::Start()
	{
		for (unsigned int i = 0; i < m_EntitiesInitial.size(); i++)
		{
			m_EntitiesInitial[i]->Start();
		}

		m_EntitiesInitial.clear();
	}

	void EntityManager::Update()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->isActive() == false)
			{
				EraseEntity(m_Entities[i]->GetUniqueID());
			}
			else if (m_Entities[i]->enabled == true)
			{
				m_Entities[i]->Update();
			}
		}
	}

	void EntityManager::FixedUpdate()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->enabled == true)
			{
				m_Entities[i]->FixedUpdate();
			}
		}
	}

	void EntityManager::Refresh()
	{

	}

	void EntityManager::AddEntity(Object * entity)
	{
		std::shared_ptr<Object> sharedPtr{ entity };

		std::shared_ptr<Scene> scene = SceneManager::GetInstance().GetLatestActiveScene();
		if (scene != nullptr)
		{
			scene->GetRootsGameObject()->push_back(sharedPtr);
		}

		m_Entities.push_back(sharedPtr);

		m_EntitiesInitial.push_back(sharedPtr);
	}

	void EntityManager::EraseEntity(unsigned int objectID)
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->GetUniqueID() == objectID)
			{
				m_Entities.erase(m_Entities.begin() + i);
			}
		}
	}

	Object * EntityManager::CloneEntity(Object * entity)
	{
		return nullptr;
	}
}