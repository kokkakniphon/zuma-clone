#pragma once

#include <vector>
#include <memory>
#include "Object.h"
#include "SceneManager.h"

namespace DucklingsEngine
{

	class EntityManager
	{
	private:
		std::vector<std::shared_ptr<Object>> m_Entities;
		std::vector<std::shared_ptr<Object>> m_EntitiesInitial;

	public:
		EntityManager() = default;
		virtual ~EntityManager() = default;

		void Draw();
		void Start();
		void Update();
		void FixedUpdate();
		void Refresh();

		void AddEntity(Object* entity);
		void EraseEntity(unsigned int objectID);
		Object* CloneEntity(Object* entity);
	};
}