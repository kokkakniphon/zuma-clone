#include "SceneManager.h"

namespace DucklingsEngine
{
	SceneManager* SceneManager::s_Instance = nullptr;

	SceneManager::~SceneManager()
	{
		buildSettingScenes.clear();
		loadedScenes.clear();
	}

	void SceneManager::Init()
	{
		// Set build index
		for (unsigned int i = 0; i < buildSettingScenes.size(); i++)
		{
			std::cout << "Build Index: " << i << " " << buildSettingScenes[i]->name << std::endl;
			buildSettingScenes[i]->SetBuildIndex(i);
		}

		if(buildSettingScenes.size() > 0)
			LoadScene(0);
		else
		{
			std::cout << "There is no scene in the build settings to be loaded" << std::endl;
		}
	}

	std::shared_ptr<Scene> SceneManager::GetFirstActiveScene()
	{
		if (loadedScenes.size() > 0)
			return loadedScenes[0];

		return nullptr;
	}

	std::shared_ptr<Scene> SceneManager::GetLatestActiveScene()
	{
		if (loadedScenes.size() > 0)
			return loadedScenes[loadedScenes.size() - 1];

		return nullptr;
	}

	void SceneManager::LoadScene(std::string sceneName)
	{
		for (auto& scene : buildSettingScenes)
		{
			if (scene->name == sceneName && scene->isLoaded == false)
			{
				loadedScenes.push_back(scene);
				scene->LoadScene();
				break;
			}
		}

		return;
	}

	void SceneManager::LoadScene(int sceneBuildIndex)
	{
		for (auto scene : buildSettingScenes)
		{
			if (scene->GetBuildIndex() == sceneBuildIndex && scene->isLoaded == false)
			{
				loadedScenes.push_back(scene);
				scene->LoadScene();
				break;
			}
		}

		return;
	}

	bool SceneManager::UnloadScene(std::string sceneName)
	{
		for (unsigned int i = 0; i < loadedScenes.size(); i++)
		{
			if (loadedScenes[i]->name == sceneName && loadedScenes[i]->isLoaded == true)
			{
				if (loadedScenes[i]->UnloadScene() == true)
				{
					loadedScenes.erase(loadedScenes.begin() + i);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	bool SceneManager::UnloadScene(int sceneBuildIndex)
	{
		for (unsigned i = 0; i < loadedScenes.size(); i++)
		{
			if (loadedScenes[i]->GetBuildIndex() == sceneBuildIndex && loadedScenes[i]->isLoaded == true)
			{
				if (loadedScenes[i]->UnloadScene() == true)
				{
					loadedScenes.erase(loadedScenes.begin() + i);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	void SceneManager::Clean()
	{
		delete s_Instance;
	}



}