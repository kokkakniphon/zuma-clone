#pragma once

#include <vector>
#include "Scene.h"

namespace DucklingsEngine
{
	class SceneManager
	{
	private:
		static SceneManager* s_Instance;

		//
		// Summary:
		//     Number of Scenes in Build Settings.
		static int sceneCountInBuildSettings;
		//
		// Summary:
		//     The total number of currently loaded Scenes.
		static int sceneCount;

		std::vector<std::shared_ptr<Scene>> buildSettingScenes;
		std::vector<std::shared_ptr<Scene>> loadedScenes;

	public:
		SceneManager() = default;
		~SceneManager();

		inline static SceneManager& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new SceneManager();
			}
			return *s_Instance;
		}

		inline void AddSceneToBuildSettings(std::shared_ptr<Scene> scene)
		{
			buildSettingScenes.push_back(scene);
		}

		void Init();

		//
		// Summary:
		//     Return the number of Scenes in Build Settings.
		inline int GetSceneCountInBuildSettings() { return sceneCountInBuildSettings; }
		//
		// Summary:
		//     The total number of currently loaded Scenes.
		inline int GetSceneCount() { return sceneCount; }

		//
		// Summary:
		//     Gets the first active Scene.
		//
		// Returns:
		//     The active Scene.
		std::shared_ptr<Scene> GetFirstActiveScene();
		//
		// Summary:
		//     Gets the latest active Scene.
		//
		// Returns:
		//     The active Scene.
		std::shared_ptr<Scene> GetLatestActiveScene();

		//
		// Summary:
		//     Loads the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		//
		//   mode:
		//     Allows you to specify whether or not to load the Scene additively. See SceneManagement.LoadSceneMode
		//     for more information about the options.
		void LoadScene(std::string sceneName);
		//
		// Summary:
		//     Loads the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		//
		//   mode:
		//     Allows you to specify whether or not to load the Scene additively. See SceneManagement.LoadSceneMode
		void LoadScene(int sceneBuildIndex);
		//
		// Summary:
		//     Destroys all GameObjects associated with the given Scene and removes the Scene
		//     from the SceneManager.
		//
		// Parameters:
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to unload.
		//
		//   sceneName:
		//     Name or path of the Scene to unload.
		//
		//   scene:
		//     Scene to unload.
		//
		// Returns:
		//     Returns true if the Scene is unloaded.
		bool UnloadScene(std::string sceneName);
		//
		// Summary:
		//     Destroys all GameObjects associated with the given Scene and removes the Scene
		//     from the SceneManager.
		//
		// Parameters:
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to unload.
		//
		//   sceneName:
		//     Name or path of the Scene to unload.
		//
		//   scene:
		//     Scene to unload.
		//
		// Returns:
		//     Returns true if the Scene is unloaded.
		bool UnloadScene(int sceneBuildIndex);

		void Clean();
	};
}

