#include "SpriteManager.h"

namespace DucklingsEngine
{
	SpriteManager* SpriteManager::s_Instance = nullptr;

	void SpriteManager::Clean()
	{
		m_Sprites.clear();

		delete s_Instance;
	}

	Sprite * SpriteManager::GetSprite(std::string id)
	{
		return (m_Sprites.count(id) > 0) ? m_Sprites[id].get() : nullptr;
	}

	void SpriteManager::AddSprite(Sprite * sprite, std::string name)
	{
		if (name == "") name = sprite->textureID;
		std::unique_ptr<Sprite> uniquePtr{ sprite };

		m_Sprites[name] = std::move(uniquePtr);
	}


}