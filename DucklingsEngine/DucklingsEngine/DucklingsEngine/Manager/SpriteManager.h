#pragma once

#include <map>
#include <string>
#include "Sprite.h"

namespace DucklingsEngine
{
	class SpriteManager
	{
	private:
		static SpriteManager* s_Instance;
		std::map<std::string, std::unique_ptr<Sprite>> m_Sprites;

	public:
		SpriteManager() = default;
		~SpriteManager() = default;

		void Clean();

		Sprite* GetSprite(std::string id);
		void AddSprite(Sprite * sprite, std::string name = "");

		inline static SpriteManager& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new SpriteManager();
			}
			return *s_Instance;
		}
	};

}

