#pragma once

#include "Vector2D.h"
#include <string>

namespace DucklingsEngine
{
	struct Bounds
	{
		Bounds() { center = Vector2(); size = Vector2(); }

		//
		// Summary:
		//     Creates a new Bounds.
		//
		// Parameters:
		//   center:
		//     The location of the origin of the Bounds.
		//
		//   size:
		//     The dimensions of the Bounds.
		Bounds(Vector2 center, Vector2 size) { this->center = center; this->size = size; }

		//
		// Summary:
		//     The extents of the Bounding Box. This is always half of the size of the Bounds.
		Vector2 extents;
		//
		// Summary:
		//     The total size of the box. This is always twice as large as the extents.
		Vector2 size;
		//
		// Summary:
		//     The center of the bounding box.
		Vector2 center;
		//
		// Summary:
		//     The minimal point of the box. This is always equal to center-extents.
		Vector2 min;
		//
		// Summary:
		//     The maximal point of the box. This is always equal to center+extents.
		Vector2 max;

		//
		// Summary:
		//     The closest point on the bounding box.
		//
		// Parameters:
		//   point:
		//     Arbitrary point.
		//
		// Returns:
		//     The point on the bounding box or inside the bounding box.
		Vector2 ClosestPoint(Vector2 point);
		//
		// Summary:
		//     Is point contained in the bounding box?
		//
		// Parameters:
		//   point:
		bool Contains(Vector2 point);
		//
		// Summary:
		//     Grows the Bounds to include the point.
		//
		// Parameters:
		//   point:
		void Encapsulate(Vector2 point);
		//
		// Summary:
		//     Grow the bounds to encapsulate the bounds.
		//
		// Parameters:
		//   bounds:
		void Encapsulate(Bounds bounds);
		//
		// Summary:
		//     Expand the bounds by increasing its size by amount along each side.
		//
		// Parameters:
		//   amount:
		void Expand(float amount);
		//
		// Summary:
		//     Expand the bounds by increasing its size by amount along each side.
		//
		// Parameters:
		//   amount:
		void Expand(Vector2 amount);
		//
		// Summary:
		//     Does another bounding box intersect with this bounding box?
		//
		// Parameters:
		//   bounds:
		bool Intersects(Bounds bounds);
		//
		// Summary:
		//     The smallest squared distance between the point and this bounding box.
		//
		// Parameters:
		//   point:
		float SqrDistance(Vector2 point);
		//
		// Summary:
		//     Returns a nicely formatted string for the bounds.
		//
		// Parameters:
		//   format:
		std::string ToString();

		//static bool operator == (Bounds lhs, Bounds rhs) { return false; }
		//static bool operator != (Bounds lhs, Bounds rhs) { return false; }
	};
}