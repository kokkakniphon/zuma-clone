# ZUMA Clone

**ZUMA Clone is a student game project created using early version of DucklingEngine. It showcases the engine's capabilities and provides a practical example of its usage.**

![ZUMA Preview](./Readme/DucklingEngine_2_Preview.png)

[ZUMA Demo](https://youtu.be/KNwu3tO3Rhs)

# DucklingEngine

**DucklingEngine** is a versatile, custom 2D game engine written in C++ and built on top of the Simple DirectMedia Layer (SDL). It provides a strong foundation for developing 2D games and interactive applications. DucklingEngine is licensed under the Apache 2.0 license.

**EggYegg Preview**

- **EggYegg Game** is another example that use the full version of DucklingEngine. [EggYegg Game Project](https://bitbucket.org/kokkakniphon/eggyegg/src/master/)

![EggYegg Preview](./Readme/DucklingEngine_Preview.png)

## Controls

- **G** : Start/Stop balls

- **LSHIFT + E** : Toggle enable path visual

- **LSHIFT + D** : Toggle enable path editor

- **LSHIFT + C** : Toggle close path loop

- **LSHIFT + A** : Toggle auto set point-anchor

- **LSHIFT + C** : Save path data

### Credits

- **Niphon Prasopphol**
  - LinkedIn: [Niphon Prasopphol's Profile](https://www.linkedin.com/in/kokkak-niphon-48a498204/)

## Note
- **This game project is created during Year 2 at KMUTT** so the code is **NOT** clean and efficient.

## License

This project is distributed under the terms of the Apache 2.0 license.
